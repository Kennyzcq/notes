## systemd-networkd

#### Reset UUID (this is used for IP address assignments instead of Mac address):
To reset UUID, clear the contents of /etc/machine-id, but do not delete it. Ensure /var/lib/dbus/machine-id is a symbolic link to /etc/machine-id. Then, reboot the instance.
