## Proxmox

#### Useful pct commands

###### List snapshots
`pct listsnapshot <id>`

###### Delete a snapshot
`pct delsnapshot <id>`

###### Remove node from cluster
Change quorum to 1:
  `pvecm expected 1`

Remove the node:
  `pvecm delnode <nodename>`

Remove the directory for the node (ensures it's removed from GUI):
  `rm -r /etc/pve/nodes/<node_name>`

remove keys from:
  `/etc/pve/priv/authorized_keys`
  `/root/.ssh/authorized_keys` (if it exists)

#### Checklist for generating a new Ubuntu template
The following steps can be completed for creating a new template.

* Install Ubuntu
* Remove ISO image from VM
* Add cloud-init drive, set user and DNS domain
* Regenerate cloud-init drive
* Enable qemu-agent in VM options
* `sudo apt install cloud-init qemu-guest-agent`
* run ansible provision as root
* `rm /root/.ansible /home/jay/ansible_bootstrap`
* reset jay and root password to initial default
* rm /etc/machine-id
* touch /etc/machine-id (this file must exist, but be blank, otherwise it won't regen)
* `rm /var/lib/dbus/machine-id`
* `ln -s /etc/machine-id /var/lib/dbus/machine-id`
* Edit /etc/cloud/cloud.cfg, remove unnecessary configs
* logout root
* Run /usr/local/bin/image_prep.sh (to clear log files, etc)
* shut down
* Create template

#### Useful articles

###### ZFS memory adjustment tips:
https://dlford.io/memory-tuning-proxmox-zfs/
