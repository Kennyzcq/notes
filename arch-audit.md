## arch-audit

###### Show vulnerable packages
  `arch-audit`

###### Show only vulnerable package names and their versions
  `arch-audit -q`

###### Show only packages that have already been fixed
  `arch-audit --upgradable --quiet`

###### Print only package names and their associated CVE's
  `arch-audit -uf "%n|%c"`
