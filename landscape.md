## Landscape

#### Install repository
  `# add-apt-repository -u ppa:landscape/18.03`

#### Install Landscape
  `# apt install landscape-server-quickstart`
