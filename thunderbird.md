## Thunderbird

###### Make text in compose window wrap to window width:
    In the configuration editor, change:
    mailnews.wraplength = 0

###### Change default sort order
    In the configuration editor, change:
    mailnews.default_sort_order = 2
